import random
from hangman_words import word_list
from hangman_art import stages, logo


def check_guess(word, guessed_letter, guessed_word):
    correct = False
    for i in range(len(word)):
        if word[i] == guessed_letter:
            correct = True
            guessed_word[i] = guessed_letter
    return [guessed_word, correct]


def setup_guessed_word(word):
    guessed_word = []
    for _ in word:
        guessed_word.append("_")
    return guessed_word


# Init
random.seed()
chosen_word = random.choice(word_list)
display = setup_guessed_word(chosen_word)
lives = 6

# debug
print(logo)

game_running = True
while game_running:
    guess = input("Please guess a letter: ").lower()
    check = check_guess(chosen_word, guess, display)
    display = check[0]
    if not check[1]:
        print("Wrong, you loose a life.")
        lives -= 1
    print(stages[lives])
    print(f"Lives left: {lives}")
    print(display)

    if lives < 1:
        print("You loose")
        game_running = False

    if display == list(chosen_word):
        print("You win!")
        game_running = False

