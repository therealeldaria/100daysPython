from config import Config
from common import generate_password
from time import sleep
from datetime import datetime
import requests


# Load API config
api_conf = Config('../api_config.json', autosave=True)
api_conf.load()


def create_user(username: str) -> bool:
    """
    Create user in piexe.la website.

    :param username: The desired username to create
    :type username: str
    :return: True if all was ok
    :rtype: bool
    """
    if api_conf.get(username, "apis", "pixe.la", "users", default=False):
        raise ValueError(f"User '{username}' is already locally created.")
    else:
        token = generate_password(length=25, symbols=False)
        api_conf.create("token", token, "apis", "pixe.la", "users", username)

    api_url = api_conf.get("url", "apis", "pixe.la")
    url = f"{api_url}/v1/users"

    parameters = {
        "token": token,
        "username": username,
        "agreeTermsOfService": "yes",
        "notMinor": "yes"
    }
    response = requests.post(url=url, json=parameters)
    response_data = response.json()

    if response_data['isSuccess']:
        api_conf.save()
        return True
    else:
        api_conf.delete(username, "apis", "pixe.la", "users", recursive=True)
        api_conf.save()
        raise ChildProcessError(f"API returned an error {response.text}")


def delete_user(username: str) -> bool:
    """
    Delete user from pixe.la website

    :param username: The username to delete
    :type username: str
    :return: Tuple with result
    :rtype: tuple[int, str]
    """
    token = api_conf.get("token", "apis", "pixe.la", "users", username, default=False)
    if not token:
        raise ValueError(f"User '{username}' not found in local config.")

    api_url = api_conf.get("url", "apis", "pixe.la")
    url = f"{api_url}/v1/users/{username}"

    headers = {
        "X-USER-TOKEN": token
    }

    response = requests.delete(url=url, headers=headers)
    response_data = response.json()

    if response_data['isSuccess']:
        api_conf.delete(username, "apis", "pixe.la", "users", recursive=True)
        api_conf.save()
        return True
    else:
        raise ChildProcessError(f"API returned an error: {response.text}")


def create_graph(
        username:str,
        graph_id: str,
        graph_name: str,
        graph_unit: str,
        graph_type: str,
        graph_color: str,
        graph_tz: str = "Etc/UTC") -> bool:
    """
    Method to create graph in pixe.la website.

    :param username: The username to create the graph for.
    :param graph_id: unique ID for graph.
    :param graph_name: Name for graph
    :param graph_unit: Unit that graph will be.
    :param graph_type: Datatype int or float
    :param graph_color: Color of graph.
    :param graph_tz: Timezone Defaults to UTC
    :return: bool
    """
    token = api_conf.get("token", "apis", "pixe.la", "users", username, default=False)
    if not token:
        raise ValueError(f"User '{username}' not found in local config.")
    graph = api_conf.get(graph_id, "apis", "pixe.la", "users", username, "graphs", default=False)
    if graph:
        raise ValueError(f"Graph with ID: '{graph_id}' is already in local config.")
    else:
        api_conf.create("Name", graph_name, "apis", "pixe.la", "users", username, "graphs", graph_id)

    api_url = api_conf.get("url", "apis", "pixe.la")
    url = f"{api_url}/v1/users/{username}/graphs"

    parameters = {
        "id": graph_id,
        "name": graph_name,
        "unit": graph_unit,
        "type": graph_type,
        "color": graph_color,
        "timezone": graph_tz
    }

    headers = {
        "X-USER-TOKEN": token
    }

    response = requests.post(url=url, json=parameters, headers=headers)
    response_data = response.json()

    if response_data['isSuccess']:
        api_conf.save()
        return True
    else:
        api_conf.delete(graph_id, "apis", "pixe.la", "users", username, "graphs", recursive=True)
        api_conf.save()
        raise ChildProcessError(f"API returned an error: {response.text}")


def create_pixel(username: str, graph_id: str, date: str, quantity: str, optional: str = None, retries: int = 4) -> bool:
    """
    Create a pixel in a graph at pixe.la website
    :param username: Username to use
    :type username: str
    :param graph_id: Graph ID to create the pixel in
    :type graph_id: str
    :param date: The date you want this to be created at. YYYYmmdd.
    :type date: str
    :param quantity: Amount of work
    :type quantity: str
    :param optional: Optional information. Defaults to None
    :type optional: str
    :param retries: How many time to retry in case API rejects. Defaults to 4
    :type retries: int
    :return: bool
    """
    token = api_conf.get("token", "apis", "pixe.la", "users", username, default=False)
    if not token:
        raise ValueError(f"User '{username}' not found in local config.")
    if not api_conf.get(graph_id, "apis", "pixe.la", "users", username, "graphs", default=False):
        raise ValueError(f"Graph with ID: '{graph_id}' not found in local config.")

    api_url = api_conf.get("url", "apis", "pixe.la")
    url = f"{api_url}/v1/users/{username}/graphs/{graph_id}"

    parameters = {
        "date": date,
        "quantity": quantity,
        "optionalData": optional
    }

    headers = {
        "X-USER-TOKEN": token
    }

    for attempt in range(retries):
        response = requests.post(url=url, json=parameters, headers=headers)
        response_data = response.json()

        if response_data['isSuccess']:
            return True
        elif response_data['isRejected']:
            if attempt == retries - 1:
                raise ChildProcessError(f"API request was rejected after {retries} attempts.")
            sleep(2)
        else:
            raise ChildProcessError(f"API returned an error: {response.text}")

