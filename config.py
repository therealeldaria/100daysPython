import json
from pathlib import Path
from typing import Union, Any


class Config:
    """
    A configuration management class.

    This class allows you to manage, save and load configuration data from a JSON file.

    :param filepath: The path to the JSON file.
    :type filepath: Path
    :param autosave: Enable auto saving when using set.
    :type autosave: bool
    """

    def __init__(self, filepath: Union[str, Path], autosave: bool = False):
        """
        Initializes a new instance of the Config class.

        :param filepath: The Path to the JSON file.
        :type filepath: Union[str, Path]
        :param autosave: Should the system autosave. Defaults to False.
        :type autosave: bool
        """
        self.filepath = Path(filepath)
        if self.filepath.is_dir():
            raise ValueError(f"The provided path {self.filepath} points to a directory, not a file.")
        self.data = {}
        self.autosave = autosave

    def create(self, key: str, value: Any, *path: str) -> None:
        """
        Create a new key-value pair in the configuration. Raises an error if the key already exists.

        :param key: The primary key for the data.
        :type key: str
        :param value: The value to set.
        :type value: Any
        :param path: Optional nested keys to the target location.
        :type path: str
        """
        target = self.data
        for p in path:
            if p not in target:
                target[p] = {}
            target = target[p]

        if key in target:
            raise ValueError(f"The key '{key}' already exists at path: {path}. Use 'set' method to update its value.")
        target[key] = value

        if self.autosave:
            self.save(autosave=True)

    def delete(self, key: str, *path: str, recursive: bool = False) -> None:
        """
        Delete a key-value pair from the configuration.

        This method allows for the deletion of both top-level and nested keys.
        If the target key contains nested data, the recursive parameter must be set to True
        to confirm the deletion.

        :param key: The primary key to delete.
        :type key: str
        :param path: Optional zero or more nested keys leading to the target location.
        :type path: str
        :param recursive: If True, confirms the deletion of keys with nested data.
                          Default is False.
        :type recursive: bool
        """
        target = self.data
        for p in path:
            target = target.get(p, {})
        if not recursive and isinstance(target.get(key), dict) and target.get(key):
            raise ValueError(f"Key '{key}' contains nested data. Set 'recursive=True' to delete.")
        elif key in target:
            del target[key]
        else:
            raise ValueError(f"The key '{key}' does not exist in the given path.")

        if self.autosave:
            self.save(autosave=True)

    def set(self, key: str, value: Any, *path: str) -> None:
        """
        Set (update) a value in the configuration. The key should already exist.

        :param key: The primary key for the data.
        :type key: str
        :param value: The value to set.
        :type value: Any
        :param path: Optional nested keys to the target location.
        :type path: str
        """
        target = self.data
        for p in path:
            if p not in target:
                raise ValueError(f"The path '{path}' does not exist the configuration.")
            target = target[p]

        if key not in target:
            raise ValueError(f"The key '{key}' does not exist at path: {path}. Use 'create' method to add a new key.")

        target[key] = value

        if self.autosave:
            self.save(autosave=True)

    def get(self, key: str, *path: str, default: Any = None) -> Any:
        """
        Retrieve a value from the configuration.

        :param key: The primary key for the data.
        :type key: str
        :param path: Optional Zero or more nested keys leading to the target location.
        :type path: str
        :param default: Value to return if the key doesn't exist.
                        If not provided, an exception will be raised on missing keys.
        :type default: Any, optional
        :return: The retrieved value or the default if the key is not found.
        :rtype: Any
        """
        target = self.data
        for p in path:
            if p not in target:
                if default is None:
                    raise ValueError(f"The path '{p}' does not exist in the configuration.")
                return default
            target = target[p]
        if key not in target:
            if default is None:
                raise ValueError(f"The key '{key}' does not exist at path: {path}.")
            return default

        return target.get(key, default)

    def save(self, autosave: bool = False) -> None:
        """
        Save the current configuration data to the JSON file.

        If autosave is set to True then a temporary swapfile will be used.
        It will be named '.<filename>.swp' and will be in the same path as the original file.

        :param autosave: Define if this is an autosave. Default is False
        :type autosave: bool
        """
        save_file = self.filepath if not autosave else self.filepath.parent / f".{self.filepath.name}.swp"
        try:
            save_file.parent.mkdir(parents=True, exist_ok=True)
            with save_file.open('w') as f:
                json.dump(self.data, f, indent=4)
            if not autosave:
                swap_file = self.filepath.parent / f".{self.filepath.name}.swp"
                if swap_file.exists():
                    swap_file.unlink()
        except IOError as e:
            raise IOError(f"Couldn't save data to {save_file}") from e
        except json.JSONDecodeError as e:
            raise ValueError(f"Malformed JSON detected while saving to {save_file}") from e

    def load(self) -> None:
        """
        Load the configuration data from JSON file.
        If a swap file exist, load that instead and print a warning to console.
        """
        swap_file = self.filepath.parent / f".{self.filepath.name}.swp"
        if swap_file.exists():
            print("Warning: Loading configuration from swap file due to an incomplete save.")
            load_file = swap_file
        else:
            load_file = self.filepath

        try:
            with load_file.open('r') as f:
                self.data = json.load(f)
        except FileNotFoundError as e:
            raise FileNotFoundError(f"File not found, please verify filename and Path") from e
        except IOError as e:
            raise IOError(f"Couldn't load data from {load_file}") from e
        except json.JSONDecodeError as e:
            raise ValueError(f"Error: Malformed JSON detected in {load_file}") from e

