from tkinter import Tk, Label, Button, Entry


def m_to_km(m):
    return round(float(m) * 1.609344, 2)


def calculate():
    label_result.config(text=m_to_km(miles_entry.get()))


window = Tk()
window.title("Miles converter")
window.config(padx=20, pady=20)

miles_entry = Entry()
miles_entry.grid(column=1, row=0)

label_miles = Label(text="Miles")
label_miles.grid(column=2, row=0)

label_is_equal = Label(text="is equal to")
label_is_equal.grid(column=0, row=1)

label_result = Label(text=0)
label_result.grid(column=1, row=1)

label_km = Label(text="Km")
label_km.grid(column=2, row=1)

but_calc = Button(text="Calculate", command=calculate)
but_calc.grid(column=1, row=2)

window.mainloop()
