from tkinter import *


def button_clicked():
    print("I got clicked")
    my_label.config(text=my_input.get())


window = Tk()
window.title("My first GUI app")
window.minsize(width=500, height=300)
window.config(padx=20, pady=20)

# Label
my_label = Label(text="I'm a labelis", font=("Arial", 24, "bold"))
my_label.grid(column=0, row=0)


# Button

my_button = Button(text="Click me", command=button_clicked)
my_button.grid(column=1, row=1)

my_butt_2 = Button(text="This does nothing")
my_butt_2.grid(column=2, row=0)


# Entry
my_input = Entry(width=10)
my_input.grid(column=3, row=2)


window.mainloop()
