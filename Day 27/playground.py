def add(*args):
    return sum(args)


# print(add(1, 2, 3, 4, 5, 6))


def calculate(n, **kwargs):
    print(kwargs)
    # for key, value in kwargs.items():
    #     print(key)
    #     print(value)
    n += kwargs["add"]
    n *= kwargs["multiply"]
    return n


# print(calculate(2, add=3, multiply=5))

class Car:
    def __init__(self, **kwargs):
        self.make = kwargs.get("make")
        self.model = kwargs.get("model")
        self.colour = kwargs.get("colour")
        self.seats = kwargs.get("seats")


my_car = Car(make="Hyundai", model="Ioniq", colour="Blue", seats="5")
print(my_car.model)
