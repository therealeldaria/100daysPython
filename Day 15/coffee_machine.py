menus = {
    "coffee_menu": {
        "espresso": {
            "ingredients": {
                "water": 50,
                "coffee": 18,
            },
            "cost": 1.5,
            "hidden": False,
            "dispensed": 0,
        },
        "latte": {
            "ingredients": {
                "water": 200,
                "milk": 150,
                "coffee": 24,
            },
            "cost": 2.5,
            "hidden": False,
            "dispensed": 0,
        },
        "cappuccino": {
            "ingredients": {
                "water": 250,
                "milk": 100,
                "coffee": 24,
            },
            "cost": 3.0,
            "hidden": False,
            "dispensed": 0,
        },
        "-------------": {
            "hidden": False
        },
        "system": {
            "hidden": True
        },
        "off": {
            "hidden": True
        },

    },
    "system_menu": {
        "report": {
            "hidden": False,
        },
        "coins": {
            "hidden": False,
        },
        "back": {
            "hidden": False
        }
    },
}
questions = {
    "coffee_selection": {
        "type": "menu",
        "text": "Please select which beverage you would like: ",
        "extra": "coffee_menu",
    },
    "system_menu": {
        "type": "menu",
        "text": "Please select system function: ",
        "extra": "system_menu",
    },
    "continue": {
        "type": "yesno",
        "text": "Do you want to continue? Type 'yes' or 'no': ",
        "extra": ["yes", "no"]
    },
}
resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}
coins = {
    "quarters": {
        "value": 0.25,
        "amount": 5,
    },
    "dimes": {
        "value": 0.1,
        "amount": 2,
    },
    "nickles": {
        "value": 0.05,
        "amount": 46,
    },
    "pennies": {
        "value": 0.01,
        "amount": 100,
    },
}


def get_menu(menu=""):
    """show_menu(menu) Function to show a menu entry.

    Will return a String with the menu of the specified menu parameter. Takes a single String(str) argument 'menu'.
    The argument should be the name of the menu to show.

    """
    # function body
    the_menu = ""
    for menu_item in menus[menu]:
        if not menus[menu][menu_item]["hidden"]:
            the_menu += f"{menu_item.capitalize()}\n"
    return the_menu


def ask_input(question=""):
    """ask_input(question) Function to ask user for input and verify correct entry.

    Single String(str) argument to define which question to ask user.

    Returns the answer.
    """
    # Function body
    reply_options = []
    reply = ""
    reply_text = questions[question]["text"]
    question_type = questions[question]["type"]
    extra = questions[question]["extra"]
    if question_type == "menu":
        for menu_item in menus[extra]:
            reply_options.append(menu_item)
    elif question_type == "yesno":
        reply_options = extra
    else:
        return False
    while reply not in reply_options:
        reply = input(reply_text).lower()
        if reply not in reply_options:
            print(f"Invalid selection, '{reply}'.")
    return reply


def insert_coin(amount=0.0):
    """insert_coin(amount) Function to ask user to insert needed coins.

    Single Float with the amount user should insert.
    Returns change on to much inserted.
    returns True if user inserted enough coins, otherwise False.
    """
    while True:
        inserted_coins = {}
        print(f"This beverage cost ${amount}:")
        for coin in coins:
            inserted_coins[coin] = int(input(f"Please insert {coin}: "))
        coin_value = get_coin_value(inserted_coins)
        if coin_value < amount:
            print("Insufficient coins inserted.")
            if ask_input("continue") == "yes":
                print("Inserted coins refunded, please try again.")
            else:
                print("Aborting purchase, inserted coins refunded.")
                return False
        else:
            add_coins(added_coins=inserted_coins)
            if coin_value > amount:
                change = coin_value - amount
                print(pay_change(change))
            return True


def get_coin_value(coins_to_calculate={}):
    """get_coin_value(coins_to_calculate) Function to calculate Coin value

    Single argument with dictionary of coins to calculate value of.
    Returns value of coins."""
    value = 0
    for coin in coins_to_calculate:
        coin_value = coins[coin]["value"]
        coin_amount = coins_to_calculate[coin]
        value = value + round(coin_value * coin_amount, 2)
    return value


def check_resources(check_item=""):
    """check_resources(check_type) Function to check for available resources.

    Single String(str) argument to define which beverage to check for resources.

    Returns True if resources are available for selected beverage, otherwise a list with what is missing.
    If check_Type is set to "all" then a Dictionary is returned containing a list of available resources.
    """
    if check_item == "all":
        return resources
    else:
        ingredient_avail = []
        for ingredient in menus["coffee_menu"][check_item]["ingredients"]:
            if menus["coffee_menu"][check_item]["ingredients"][ingredient] > resources[ingredient]:
                ingredient_avail.append(ingredient)
        if len(ingredient_avail) == 0:
            return True
        else:
            return ingredient_avail


def remove_resources(resources2rm={}):
    """remove_resources(resource) Function to remove resources used in a beverage.

    Dictionary Argument with the resources to remove.
    """
    for ingredient in resources2rm:
        resources[ingredient] -= resources2rm[ingredient]


def dispense(beverage=""):
    """dispense(beverage) Function to dispense beverage

    Single String(str) argument with beverage to dispense
    returns True if dispensing succeeded
    returns False is dispensing failed.
    """
    resource_avail = check_resources(beverage)
    # If not enough resources, print which resources are missing
    if type(resource_avail) == list:
        output = "Sorry there is not enough "
        for i in range(len(resource_avail) - 1, -1, -1):
            if i == 0:
                if len(resource_avail) >= 2:
                    output += f" or {resource_avail[i]}"
                else:
                    output += resource_avail[i]
            elif i > 1 and len(resource_avail) > 2:
                output += f"{resource_avail[i]}, "
            else:
                output += resource_avail[i]
        output += f" to dispense {beverage}."
        print(output)
        print(return_funds(beverage))
    else:
        remove_resources(menus["coffee_menu"][beverage]["ingredients"])
        menus["coffee_menu"][beverage]["dispensed"] += 1
        print(f"Dispensing {beverage}.")


def add_coins(added_coins={}):
    """add_funds(added_coins) Function to add coins to inventory.

    added_coins should be a dictionary with each type of coin inserted.
    returns value of inserted coins.
    """
    value = 0
    for coin_type in added_coins:
        coin_value = added_coins[coin_type] * coins[coin_type]["value"]
        value += coin_value
        coins[coin_type]["amount"] += added_coins[coin_type]
    return round(value, 2)


def return_funds(beverage=""):
    """return_funds(beverage) Function to generate return funds text if beverage dispensing failed

    Single String(str) argument with the beverage to return funds for.
    """
    value = menus["coffee_menu"][beverage]["cost"]
    return_value = remove_coins(value)
    return_text = f"Refunding ${value} for beverage {beverage.capitalize()}.\n"
    for coin in return_value:
        if return_value[coin] > 0:
            return_text += f"- {return_value[coin]} {coin}\n"
    return return_text


def pay_change(amount=0.0):
    """pay_change(amount) Function to pay out change.

    Single Float with amount to be paid out.
    Returns text for paying change.
    """
    return_text=f"Change ${round(amount, 2)}.\n"
    return_value = remove_coins(amount)
    for coin in return_value:
        if return_value[coin] > 0:
            return_text += f"- {return_value[coin]} {coin}\n"
    return return_text


def remove_coins(value=0.0):
    """remove_coins(value) Function to calculate which coins to remove based on value.

    Single float argument with the value to base the calculation on.
    Will check if the coins are available, and give lower valued coins of higher value not available.
    For example if 0.25 is not available then it will try to give 2x0.1 and 1x0.05
    Returns Dictionary with coins removed
    """
    return_coins = {}
    for coin_type in coins:
        coin_type_value = coins[coin_type]["value"]
        whole_value = int(value / coin_type_value)
        if whole_value > coins[coin_type]["amount"]:
            whole_value = coins[coin_type]["amount"]
        return_coins[coin_type] = whole_value
        coins[coin_type]["amount"] -= whole_value
        value = round(value - (whole_value * coin_type_value), 2)
    return return_coins


def count_funds():
    """count_funds() A function to check amount of funds in inventory.

    Returns a dictionary with coins in storage and a total value.
    """
    value = 0
    coins_in_storage = {}
    for coin in coins:
        coins_in_storage[coin] = coins[coin]["amount"]
        value = value + round(coins[coin]["amount"] * coins[coin]["value"], 2)
    coins_in_storage["total"] = value
    return coins_in_storage


def coin_report():
    """coin_report() Function to generate report of Coins in inventory.

    Returns String with report.
    """
    report_data = count_funds()
    return_text = ""
    for data in report_data:
        if data == "total":
            return_text += f"Total value: ${report_data[data]}"
        else:
            return_text += f"{data} - {report_data[data]}\n"
    return return_text


def inventory_report():
    """inventory_report() Function to generate the Inventory Report

    Returns a String with Report of ingredients and Value of Coins in inventory
    """
    inventory = check_resources(check_item="all")
    coin_value = count_funds()["total"]
    return_text = ""
    for item in inventory:
        return_text += f"{item.capitalize()} - {inventory[item]}\n"
    return_text += f"Coin value: ${coin_value}.\n"
    return return_text


system_running = True
while system_running:
    print("\n---- Main Menu ----")
    print(get_menu("coffee_menu"))
    selection = ask_input("coffee_selection")
    if selection == "off":
        system_running = False
        print("Turning off coffee machine...")
    elif selection == "system":
        in_system = True
        while in_system:
            print("\n---- System Menu ----")
            print(get_menu("system_menu"))
            system_selection = ask_input("system_menu")
            if system_selection == "report":
                print(inventory_report())
            elif system_selection == "coins":
                print(coin_report())
            else:
                in_system = False
    else:
        cost = menus["coffee_menu"][selection]["cost"]
        if insert_coin(cost):
            print("")
            dispense(selection)
            print("")
