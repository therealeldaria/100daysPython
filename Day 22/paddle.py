import turtle
from turtle import Turtle


class Paddle(Turtle):
    """Class to create a paddle on the game board"""
    def __init__(self, paddle_type, paddle_side):
        """
        Parameters:
        :param paddle_type:
        Can be:
        Player
        CPU
        :param paddle_side
        Can be:
        Left
        Right
        """
        super().__init__()
        self.paddle_type = paddle_type
        self.paddle_side = paddle_side
        self.shape("square")
        self.penup()
        self.color("white")
        self.speed("fastest")
        self.left(90)
        self.shapesize(stretch_wid=1, stretch_len=5)
        if paddle_side == "Right":
            self.setposition(x=350, y=0)
        else:
            self.setposition(x=-350, y=0)

    def move_up(self):
        new_y = self.ycor() + 20
        self.setposition(self.xcor(), new_y)

    def move_down(self):
        new_y = self.ycor() - 20
        self.setposition(self.xcor(), new_y)
