from turtle import Screen, Turtle
from paddle import Paddle
from ball import Ball
from scoreboard import Score

SPEED = 40
screen = Screen()
screen.setup(width=800, height=600)
screen.bgcolor("black")
screen.title(titlestring="Pong")
#screen.tracer(0)

r_paddle = Paddle(paddle_type="Player", paddle_side="Right")
l_paddle = Paddle(paddle_type="Player", paddle_side="Left")
ball = Ball()
l_score = Score(player=l_paddle)
r_score = Score(player=r_paddle)

game_running = True


def game_loop():
    global game_running
    if game_running:
        ball.move()
        collision_detector()
        screen.update()
        screen.ontimer(game_loop, SPEED)


def collision_detector():
    y_cor = ball.ycor()
    x_cor = ball.xcor()
    y_edge = int(screen.window_height() / 2)
    x_edge = int(screen.window_width() / 2)
    if (y_cor - 17 < -abs(y_edge)) or (y_cor + 15 > y_edge):
        ball.collide("edge")
    if (x_cor < -abs(x_edge - 75) and ball.distance(l_paddle) < 50) or (x_cor > x_edge - 75 and ball.distance(r_paddle) < 50):
        ball.collide("paddle")
        ball.accelerate()
    if x_cor < -abs(x_edge) or x_cor > x_edge:
        if x_cor < -abs(x_edge):
            r_score.add_point()
        else:
            l_score.add_point()
        ball.collide("paddle")
        ball.reset_ball()



screen.listen()
screen.onkey(fun=r_paddle.move_up, key="Up")
screen.onkey(fun=r_paddle.move_down, key="Down")
screen.onkey(fun=l_paddle.move_up, key="w")
screen.onkey(fun=l_paddle.move_down, key="s")

game_loop()

screen.mainloop()
