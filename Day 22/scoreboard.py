from turtle import Turtle
ALIGNMENT = "center"
FONT = ("Comic Sans", 25, "normal")
Y_POSITION = 260
X_OFFSET = 80


class Score(Turtle):
    def __init__(self, player):
        super().__init__()
        self.current_score = 0
        self.penup()
        self.hideturtle()
        self.player = player.paddle_side
        if self.player == "Left":
            offset = -abs(X_OFFSET)
        else:
            offset = X_OFFSET
        self.setposition(x=offset, y=Y_POSITION)
        self.color("white")
        self.speed("fastest")
        self.update_score()

    def update_score(self):
        self.clear()
        self.write(f"{self.current_score}", move=False, align=ALIGNMENT, font=FONT)

    def add_point(self):
        self.current_score += 1
        self.update_score()

    def game_over(self):
        self.setposition(0, 0)
        self.write(f"Game Over", move=False, align=ALIGNMENT, font=FONT)
