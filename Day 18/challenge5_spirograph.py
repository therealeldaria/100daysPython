import turtle
from turtle import Turtle, Screen
import random


turtle.colormode(255)

peppe = Turtle()
peppe.shape("circle")
peppe.shapesize(0.5, 0.5, 1)
peppe.resizemode("user")
peppe.speed(0)


def random_color():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    return r, g, b


for i in range(0, 360, 5):
    peppe.setheading(i)
    peppe.color(random_color())
    peppe.circle(100)

screen = Screen()
screen.exitonclick()
