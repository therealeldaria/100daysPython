from turtle import Turtle, Screen
import random

colors = ["blue", "red", "purple", "green", "black", "yellow", "cyan", "grey", "pink"]

peppe = Turtle()
peppe.shape("circle")
peppe.shapesize(0.5, 0.5, 1)
peppe.resizemode("user")

for i in range(3, 11):
    peppe.color(random.choice(colors))
    for _ in range(i):
        peppe.forward(100)
        peppe.right(360 / i)

screen = Screen()
screen.exitonclick()
