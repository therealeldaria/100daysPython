from turtle import Turtle, Screen


peppe = Turtle()
peppe.shape("circle")
peppe.color("blue")

for _ in range(4):
    peppe.forward(100)
    peppe.right(90)

screen = Screen()
screen.exitonclick()