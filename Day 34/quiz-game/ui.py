from tkinter import *
from quiz_brain import QuizBrain

THEME_BG_COLOR = "#375362"
THEME_LBL_COLOR = "#FFFFFF"
THEME_CORRECT_COLOR = "#1FD14F"
THEME_WRONG_COLOR = "#F02800"

class QuizGUI:
    def __init__(self, quiz_brain: QuizBrain):
        self.quiz = quiz_brain
        self.window = Tk()
        self.window.title("Quizzler")
        self.window.config(pady=20, padx=20, bg=THEME_BG_COLOR)
        self.img_true = PhotoImage(file='images/true.png')
        self.img_false = PhotoImage(file='images/false.png')
        self.lbl_score = Label(
            text="Score: 0",
            justify="center",
            anchor="w",
            bg=THEME_BG_COLOR,
            fg="#FFFFFF",
            font=('Ariel', 10, 'bold'))
        self.canvas = Canvas(width=300, height=250, highlightthickness=0, bg=THEME_LBL_COLOR)
        self.quiz_text = self.canvas.create_text(
            150,
            125,
            text="Quizzler",
            fill=THEME_BG_COLOR,
            font=('Ariel', 20, 'italic'),
            width=260,
        )
        self.but_true = Button(
            image=self.img_true,
            highlightthickness=0,
            bg=THEME_BG_COLOR,
            borderwidth=0,
            activebackground=THEME_BG_COLOR,
            activeforeground=THEME_BG_COLOR,
            command=self.check_true,
        )
        self.but_false = Button(
            image=self.img_false,
            highlightthickness=0,
            bg=THEME_BG_COLOR,
            borderwidth=0,
            activebackground=THEME_BG_COLOR,
            activeforeground=THEME_BG_COLOR,
            command=self.check_false,
        )
        self.lbl_score.grid(row=0, column=1, padx=20, pady=20)
        self.canvas.grid(row=1, column=0, columnspan=2, padx=20, pady=20)
        self.but_true.grid(row=2, column=0, padx=20, pady=20)
        self.but_false.grid(row=2, column=1, padx=20, pady=20)
        self.get_next_question()
        self.window.mainloop()

    def update_question_text(self, quiz_text):
        self.canvas.itemconfig(self.quiz_text, text=quiz_text)

    def get_next_question(self):
        self.canvas.config(bg=THEME_LBL_COLOR)
        if self.quiz.still_has_questions():
            q_text = self.quiz.next_question()
            self.update_buttons('normal')
        else:
            q_text = f"All done."
        self.update_question_text(q_text)

    def check_true(self):
        feedback = self.quiz.check_answer("True")
        self.answer_feedback(feedback)

    def check_false(self):
        feedback = self.quiz.check_answer("False")
        self.answer_feedback(feedback)

    def answer_feedback(self, result):
        self.update_buttons('disabled')
        if result:
            self.canvas.config(bg=THEME_CORRECT_COLOR)
            self.lbl_score.config(text=f"Score: {self.quiz.score}")
        else:
            self.canvas.config(bg=THEME_WRONG_COLOR)
        self.window.after(1000, self.get_next_question)

    def update_buttons(self, state):
        self.but_false.config(state=state)
        self.but_true.config(state=state)