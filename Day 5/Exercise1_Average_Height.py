student_heights = [180, 124, 165, 173, 189, 169, 146]


# Write your code below this row 👇
item_count = 0
item_total = 0
for item in student_heights:
  item_count += 1
  item_total += item

print(round(item_total/item_count))
