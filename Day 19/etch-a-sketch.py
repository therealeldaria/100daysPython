from turtle import Turtle, Screen

peppe = Turtle()
screen = Screen()


def move_forward():
    peppe.forward(10)


def move_backward():
    peppe.backward(10)


def turn_left():
    peppe.left(5)


def turn_right():
    peppe.right(5)


def clear_screen():
    peppe.clear()
    peppe.pu()
    peppe.home()
    peppe.pd()


screen.listen()
screen.onkey(key="w", fun=move_forward)
screen.onkey(key="s", fun=move_backward)
screen.onkey(key="a", fun=turn_left)
screen.onkey(key="d", fun=turn_right)
screen.onkey(key="c", fun=clear_screen)


screen.exitonclick()