from random import randint
from tkinter import *
from turtle import RawTurtle

root = Tk()

turtle_colours = ["Blue", "Red", "Green", "Black", "Orange", "Purple"]
turtle_selection = ""
winner = ""


def select_color():
    def onselect(evt):
        w = evt.widget
        index = int(w.curselection()[0])
        value = w.get(index)
        my_butt.config(fg=value)

    def my_butt_click():
        global turtle_selection
        turtle_selection = color_picker.get(ANCHOR)
        color_frame.destroy()
        race_turtles(turtle_selection)

    root.title("Color Picker")
    color_frame = Frame(master=root)
    color_frame.pack()

    color_label = Label(master=color_frame, pady=5, padx=5, text="Select Your Turtle.", font=("Arial", 20))
    color_label.pack(side="top")
    ls_color = Variable(value=turtle_colours)
    color_picker = Listbox(master=color_frame, bg="lightgrey", height=len(turtle_colours), listvariable=ls_color,
                           selectmode=SINGLE)
    for i in range(0, color_picker.size()):
        color_picker.itemconfig(i, {'fg': color_picker.get(i)})
    color_picker.pack(expand=True, fill=BOTH, pady=5, padx=5)
    color_picker.bind("<<ListboxSelect>>", onselect)
    my_butt = Button(master=color_frame, text="Race!", bg="lightgrey", fg="white", command=my_butt_click)
    my_butt.pack(side="bottom")


def race_turtles(color):
    root.title("Turtle Race")
    logo_frame = Frame(master=root)
    logo_frame.pack(side="top")
    race_frame = Frame(master=root)
    race_frame.pack(side="bottom")

    canvas = Canvas(master=race_frame, width=800, height=800)
    canvas.grid(padx=1, pady=1, column=0, rowspan=1, columnspan=1)

    turtles = {name: RawTurtle(canvas=canvas) for name in turtle_colours}

    race_label = Label(master=logo_frame, text="Turtle Race!", fg=color, font=("Arial", 20))
    race_label.pack(side="top")

    # Setup Turtles
    pos = -250
    for tc in turtles.items():
        tc[1].color(tc[0])
        tc[1].shape("turtle")
        tc[1].penup()
        tc[1].setposition(x=-300, y=pos)
        pos += 100

    racing = True
    while racing:
        for tc in turtles.items():
            movement = randint(1, 10)
            tc[1].forward(movement)
            if int(tc[1].position()[0]) > 380:
                global winner
                winner = tc[0]
                racing = False
                break
    for tc in turtles.items():
        if tc[0] != winner:
            tc[1].hideturtle()
        else:
            tc[1].setposition(x=0, y=0)
            if winner == turtle_selection:
                tc[1].write("You win!", move=True, font=("Arial", 20))
            else:
                tc[1].write("You lost!", move=True, font=("Arial", 20))


select_color()
root.mainloop()
