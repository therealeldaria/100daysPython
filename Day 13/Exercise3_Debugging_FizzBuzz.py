# Original Code
# for number in range(1, 101):
#   if number % 3 == 0 or number % 5 == 0:
#     print("FizzBuzz")
#   if number % 3 == 0:
#     print("Fizz")
#   if number % 5 == 0:
#     print("Buzz")
#   else:
#     print([number])


# Fixed Code
for number in range(1, 101):
    if number % 3 == 0 and number % 5 == 0:  # For FizzBuzz both statement has to be true, so 'and' instead of 'or'
        print("FizzBuzz")
    elif number % 3 == 0:  # Should be elif since it should only be evaluated if previous test was false
        print("Fizz")
    elif number % 5 == 0:  # Should be elif since it should only be evaluated if previous test was false
        print("Buzz")
    else:
        print(number)