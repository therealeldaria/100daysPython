import pandas

df = pandas.read_csv("nato_phonetic_alphabet.csv")

# Create a dictionary in this format:
df_dict = {row.letter: row.code for (index, row) in df.iterrows()}
# print(df_dict)

# Create a list of the phonetic code words from a word that the user inputs.

a = input("What would you like to spell, (q-z only)?").upper()


def get_word(letter):
    if letter.isalpha():
        word = df.code[df.letter == letter].values[0]
    elif letter == " ":
        word = "Space"
    else:
        word = ""
    return word


new_list = [get_word(letter) for letter in a]
print(new_list)
