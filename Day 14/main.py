from art import logo, vs
from game_data import data as game_data
import random


def add_trivia(current):
    if len(game_data) != len(current):
        new_trivia = random.choice(game_data)
        # If we already used this trivia, find a new one
        while new_trivia in current:
            new_trivia = random.choice(game_data)
        current.append(new_trivia)
        return current
    else:
        # No more trivia available
        return False


def win_game(score):
    print(f"Congratulations you won the game, your final score was: {score}.")
    return play_again()


def loose_game(score):
    print(f"Sorry that was the wrong answer, your final score was: {score}.")
    return play_again()


def play_again():
    again = ""
    while again != "y" and again != "n":
        again = input("Do you want to play again? Type 'y' or 'n': ").lower()
    if again == "y":
        return True
    else:
        return False


def more_followers(candidate1, candidate2):
    if candidate1['follower_count'] > candidate2['follower_count']:
        return "A"
    else:
        return "B"


def initialize_game():
    print(logo)
    init = []
    # First we initialize with two trivia added to trivia_used
    for _ in range(2):
        init = add_trivia(init)
    return init


# Init
game_running = True
trivia_used = initialize_game()

while game_running:
    # Get Trivia for first Candidate
    triviaA_name = trivia_used[-2]['name']
    triviaA_description = trivia_used[-2]['description']
    triviaA_country = trivia_used[-2]['country']
    # Get Trivia for second Candidate
    triviaB_name = trivia_used[-1]['name']
    triviaB_description = trivia_used[-1]['description']
    triviaB_country = trivia_used[-1]['country']
    # Who has more followers?
    winner = more_followers(candidate1=trivia_used[-2], candidate2=trivia_used[-1])
    # Print the vs data
    print(f"Compare A: {triviaA_name}, a {triviaA_description}, from {triviaA_country}")
    print(vs)
    print(f"Against B: {triviaB_name}, a {triviaB_description}, from {triviaB_country}")
    # Calculate and print score
    current_score = len(trivia_used) - 2
    print(f"Current score is {current_score}.")
    # Get the guess from user
    guess = ""
    while guess != "A" and guess != "B":
        guess = input("Who has more followers? Type 'A' or 'B': ").upper()
    if guess != winner:
        game_running = loose_game(current_score)
        if game_running:
            trivia_used = initialize_game()
    else:
        print("Correct answer.")
        trivia_used = add_trivia(trivia_used)
        if not trivia_used:
            # We are out of trivia, player wins.
            game_running = win_game(current_score)
            if game_running:
                trivia_used = initialize_game()
            else:
                break
