from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

mr_coffee = CoffeeMaker()
my_menu = Menu()
my_money = MoneyMachine()
menu_items = my_menu.get_items()
# Remove the last slash (/)
menu_items = menu_items.rstrip(menu_items[-1])
# Create a list of possible menu items for input verification.
possible_items = ['off', 'report'] + menu_items.split('/')

machine_on = True
while machine_on:
    selection = ""
    # Input verification
    while selection not in possible_items:
        selection = input(f"What would you like ({menu_items}): ").lower()

    if selection == "off":
        machine_on = False
    elif selection == "report":
        mr_coffee.report()
        my_money.report()
    else:
        drink_to_make = my_menu.find_drink(selection)
        if mr_coffee.is_resource_sufficient(drink_to_make):
            if my_money.make_payment(drink_to_make.cost):
                mr_coffee.make_coffee(drink_to_make)



