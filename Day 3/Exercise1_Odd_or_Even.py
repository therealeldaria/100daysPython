# 🚨 Don't change the code below 👇
number = int(input("Which number do you want to check? "))
# 🚨 Don't change the code above 👆

# Write your code below this line 👇

result = ""
if number % 2 == 0:
    result = "even"
else:
    result = "odd"
print(f"This is an {result} number.")
