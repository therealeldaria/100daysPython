# 🚨 Don't change the code below 👇
year = int(input("Which year do you want to check? "))
# 🚨 Don't change the code above 👆

# Write your code below this line 👇

leap = "Not leap"
if year % 4 == 0:
    leap = "Leap"
    if year % 100 == 0:
        leap = "Not leap"
        if year % 400 == 0:
            leap = "Leap"


print(f"{leap} year.")
