# 🚨 Don't change the code below 👇
print("Welcome to the Love Calculator!")
name1 = input("What is your name? \n")
name2 = input("What is their name? \n")
# 🚨 Don't change the code above 👆

# Write your code below this line 👇

score_name = (name1 + name2).lower()

score1 = score_name.count("t")
score1 += score_name.count("r")
score1 += score_name.count("u")
score1 += score_name.count("e")
score2 = score_name.count("l")
score2 += score_name.count("o")
score2 += score_name.count("v")
score2 += score_name.count("e")

score = int(str(score1) + str(score2))
score_text = ""
if score < 10 or score > 90:
    score_text = ", you go together like coke and mentos"
elif 40 < score < 50:
    score_text = ", you are alright together"
print(f"Your score is {score}{score_text}.")
