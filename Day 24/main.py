def create_letter(name):
    with open("Input/Letters/starting_letter.txt", "r") as file:
        letter = file.read()
        output_letter = letter.replace("[Name]", name)
        save_letter(name, output_letter)


def save_letter(name, output_letter):
    with open(f"Output/ReadyToSend/letter_for_{name}.txt", "w") as file:
        file.write(output_letter)


with open("Input/Names/invited_names.txt", "r") as input_names:
    names = input_names.readlines()
    for name in names:
        create_letter(name.strip())
