# 🚨 Don't change the code below 👇
age = input("What is your current age?")
# 🚨 Don't change the code above 👆

# Write your code below this line 👇
age = int(age)
timeleft = 90 - age

days = timeleft * 365
weeks = timeleft * 52
months = timeleft * 12

print(f"You have {days} days, {weeks} weeks, and {months} months left.")
