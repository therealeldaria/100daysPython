# If the bill was $150.00, split between 5 people, with 12% tip.

# Each person should pay (150.00 / 5) * 1.12 = 33.6
# Format the result to 2 decimal places = 33.60

# Tip: There are 2 ways to round a number. You might have to do some Googling to solve this.💪

# Write your code below this line 👇
bill_total = float(input("What was the total bill? $"))
tip_percentage = float(input("How much tip do you want to give? %"))
amount_people = int(input("How many people to split the bill? "))

ep_amount = round(bill_total * (tip_percentage / 100 + 1) / amount_people, 2)

print(f"Each person should pay: ${ep_amount}")
