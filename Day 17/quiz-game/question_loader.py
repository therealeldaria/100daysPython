from question_model import Question
import json
import requests
import urllib.parse


def decode_text(text):
    return urllib.parse.unquote(text)


class QuestionLoader:
    def __init__(self):
        self.api_url = "https://opentdb.com/api.php?"

    def load_questions(self, question_amount, difficulty):
        api_url = self.api_url
        if question_amount > 50:
            question_amount = 50
        elif question_amount < 1:
            question_amount = 1
        if difficulty >= 3:
            difficulty = "hard"
        elif difficulty == 2:
            difficulty = "medium"
        else:
            difficulty = "easy"
        api_url = f"{api_url}amount={question_amount}&difficulty={difficulty}&type=boolean&encode=url3986"
        response = requests.get(api_url)
        questions = json.loads(response.text)["results"]
        question_list = []
        for current_question in questions:
            the_q = decode_text(current_question["question"])
            the_a = decode_text(current_question["correct_answer"])
            q_object = Question(the_q, the_a)
            question_list.append(q_object)
        return question_list




