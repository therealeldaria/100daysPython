class QuizBrain:
    def __init__(self, question_list):
        self.question_number = 0
        self.question_list = question_list
        self.score = 0

    def next_question(self):
        question = self.question_list[self.question_number]
        possible_answer = ["true", "false", "t", "f", "q", "quit"]
        answer = ""
        while answer not in possible_answer:
            answer = input(f"Q.{self.question_number + 1}: {question.text}? (True/False/Quit): ").lower()
        self.question_number += 1
        self.check_answer(answer, question.answer)

    def still_has_questions(self):
        return self.question_number < len(self.question_list)

    def check_answer(self, answer, correct_answer):
        if answer == "t":
            answer = "True"
        elif answer == "f":
            answer = "False"
        elif answer == "q" or answer == "quit":
            exit()
        else:
            answer = answer.capitalize()
        if answer == correct_answer:
            self.score += 1
            print("Correct answer")
        else:
            print("Sorry, that was the wrong answer.")

        print(f"Your score is now: {self.score}/{self.question_number}.\n")


