from quiz_brain import QuizBrain
from question_loader import QuestionLoader


qLoader = QuestionLoader()
question_bank = qLoader.load_questions(question_amount=10, difficulty=1)

qBrain = QuizBrain(question_bank)

while qBrain.still_has_questions():
    qBrain.next_question()
print("Quiz completed.")
print(f"Your final score was {qBrain.score} out of {qBrain.question_number} possible.")

