from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

INITIAL_SPEED = 80
SCREEN_SIZE = (600, 600)


class Game:
    def __init__(self):
        self.screen = Screen()
        self.screen.setup(width=SCREEN_SIZE[0], height=SCREEN_SIZE[1])
        self.screen.bgcolor("white")
        self.screen.title(titlestring="My Crossing Game")
        self.screen.tracer(0)
        self.speed = INITIAL_SPEED

        self.player = Player()
        self.car_manager = CarManager(SCREEN_SIZE)
        self.scoreboard = Scoreboard(SCREEN_SIZE)

        self.screen.listen()
        self.screen.onkey(self.player.move, "Up")

        self.game_loop()

    def game_loop(self):
        self.screen.ontimer(self.game_loop, self.speed)
        if self.player.alive:
            self.car_manager.move()
            if self.car_manager.check_for_collision(self.player.position):
                self.player.kill()
            if self.player.is_level_up():
                self.scoreboard.add_point()
                self.car_manager.level_up()
        else:
            self.scoreboard.game_over()
            return
        self.screen.update()


if __name__ == '__main__':
    game = Game()
    game.screen.mainloop()
