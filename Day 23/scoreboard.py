from turtle import Turtle
ALIGNMENT = "CENTER"
FONT = ("Courier", 24, "normal")
LEVEL_Y_OFFSET = 40
LEVEL_X_OFFSET = 80
STARTING_LEVEL = 1


class Scoreboard:
    def __init__(self, screen_size):
        self.screen_size = screen_size
        self.level = STARTING_LEVEL
        self._create_scoreboard()

    def _create_scoreboard(self):
        self.score = Turtle()
        self.score.penup()
        self.score.hideturtle()
        self.score.setposition(-(self.screen_size[0] // 2) + LEVEL_X_OFFSET, (self.screen_size[1] // 2) - LEVEL_Y_OFFSET)
        self.score.color("black")
        self.score.write(f"Level: {self.level}", move=False, align=ALIGNMENT, font=FONT)

    def add_point(self):
        self.level += 1
        self._update_score()

    def _update_score(self):
        self.score.clear()
        self.score.write(f"Level: {self.level}", move=False, align=ALIGNMENT, font=FONT)

    def game_over(self):
        self.score.setposition(0, 0)
        self.score.penup()
        self.score.write(f"Game Over", move=False, align=ALIGNMENT, font=FONT)

