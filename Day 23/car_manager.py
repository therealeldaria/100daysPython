from turtle import Turtle
from random import choice, randint
from math import floor
COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MIN_SPEED = 1
STARTING_MAX_SPEED = 7
STARTING_LEVEL = 1


def calculate_sections(total_length, section_size):
    positions = []
    buffer = total_length % section_size
    num_sections = total_length // section_size

    if buffer > 0:
        buffer_position = (total_length - buffer)
        positions.append(buffer_position)

    for i in range(num_sections):
        section_position = i * section_size
        positions.append(section_position)

    return positions


# CarManager class to coordinate the traffic.
class CarManager:
    def __init__(self, screen_size):
        self.screen_size = screen_size
        self.level = STARTING_LEVEL
        self.active_lanes = []
        self._setup_lanes()

    def _setup_lanes(self):
        if len(self.active_lanes) > 0:
            self.clean_up()
        screen_height = self.screen_size[1]
        section_height = 20

        positions = calculate_sections(total_length=screen_height, section_size=section_height)
        # Reverse the order and make some space.
        positions = positions[::-1]
        for i in range(2, len(positions) - 4):
            # Make some space to rest
            if not randint(1, 8) in range(1, 4):
                lane = Lane(
                    y_pos=positions[i] - (self.screen_size[0] // 2),
                    screen_width=self.screen_size[0],
                    level=self.level
                )
                self.active_lanes.append(lane)
        print(f"Active amount of lanes: {len(self.active_lanes)}")

    def move(self):
        for lane in self.active_lanes:
            lane.move_cars(self.level)

    def level_up(self):
        self.level += 1
        self._setup_lanes()

    def check_for_collision(self, player, player_size=(20, 20), car_size=(40, 20)):
        player_x, player_y = player
        for lane in self.active_lanes:
            for car in lane.active_cars:
                car_x, car_y = car.position
                if (player_x - player_size[0] / 2 < car_x + car_size[0] / 2 and
                        player_x + player_size[0] / 2 > car_x - car_size[0] / 2 and
                        player_y - player_size[1] / 2 < car_y + car_size[1] / 2 and
                        player_y + player_size[1] / 2 > car_y - car_size[1] / 2):
                    return True
        return False

    def clean_up(self):
        for lane in self.active_lanes:
            lane.clear_lane()
        self.active_lanes = []


# Lane class to keep track of the cars in that lane.
class Lane:
    def __init__(self, y_pos, screen_width, level):
        self.y_pos = y_pos
        self.level = level
        self.screen_width = screen_width
        self.speed = randint(STARTING_MIN_SPEED + self.level, STARTING_MAX_SPEED + self.level)
        self.active_cars = {}
        self._setup_cars()

    def _setup_cars(self):
        possible_x_positions = [i - (self.screen_width // 2) for i in calculate_sections(self.screen_width, 40)]
        x_positions = []
        # Find 1 - 5 positions that are not next to each other.
        for i in range(randint(1, min(self.level, 5))):
            finding = True
            while finding:
                current_pos = choice(possible_x_positions)
                if not any(num in x_positions for num in [current_pos - 40, current_pos, current_pos + 40]):
                    x_positions.append(current_pos)
                    finding = False
        for x_pos in x_positions:
            self.add_car(x_pos)

    def add_car(self, x_pos):
        start_position = (x_pos, self.y_pos)
        car = Car(position=start_position)
        self.active_cars[car] = car

    def delete_car(self, car):
        if car in self.active_cars:
            car.remove_car()
            del self.active_cars[car]

    def move_cars(self, level):
        new_active_cars = {}
        cars_to_replace = []
        for car in self.active_cars:
            car.move(self.speed)
            if car.position[0] + 40 < -(self.screen_width // 2):
                cars_to_replace.append(car)
            else:
                new_active_cars[car] = car

        self.active_cars = new_active_cars

        for car in cars_to_replace:
            self.delete_car(car)
            self.add_car(self.screen_width // 2)

    def clear_lane(self):
        cars_to_delete = list(self.active_cars.keys())
        for car in cars_to_delete:
            self.delete_car(car)
        self.active_cars = {}


# Car class to handle each car.
class Car:
    def __init__(self, position):
        self.position = position
        self._create_car()

    def _create_car(self):
        self.car = Turtle()
        self.car.penup()
        self.car.setposition(x=self.position[0], y=self.position[1])
        self.car.shape("square")
        self.car.color(choice(COLORS))
        self.car.shapesize(stretch_len=2, stretch_wid=1)
        self.car.setheading(180)

    def move(self, speed):
        self.car.forward(speed)
        self.position = self.car.position()

    def remove_car(self):
        self.car.clear()
        self.car.hideturtle()
